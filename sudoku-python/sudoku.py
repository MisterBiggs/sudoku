import numpy as np
from puzzles import board, sol

# for x in board:
#     print(x)

new_board = board[::]

for i, v in enumerate(np.reshape(new_board, (9 * 9, 1))):
    row_i = i // 9
    col_i = i % 9
    row = new_board[row_i, :]
    col = new_board[:, col_i]

    # print(row_i)
    grid_r = row_i // 3

    grid_c = col_i // 3
    # print(grid_r,grid_c)
    grid = new_board[grid_r : grid_r + 3, grid_c : grid_c + 3]
    print(grid)


def row_check(row, val):
    return val in row


def col_check(col, val):
    return val in col


def grid_check(grid, val):
    return val in grid
